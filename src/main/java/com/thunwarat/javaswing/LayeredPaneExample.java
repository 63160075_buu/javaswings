/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.javaswing;
import javax.swing.*;  
import java.awt.*;  
/**
 *
 * @author ACER
 */
public class LayeredPaneExample extends JFrame{
    public LayeredPaneExample() {  
    super("LayeredPane Example");  
    setSize(200, 200);  
    JLayeredPane pane = getLayeredPane();    
    JButton top = new JButton();  
    top.setBackground(Color.PINK);  
    top.setBounds(20, 20, 50, 50);  
    JButton middle = new JButton();  
    middle.setBackground(Color.BLUE);  
    middle.setBounds(40, 40, 50, 50);  
    JButton bottom = new JButton();  
    bottom.setBackground(Color.GREEN);  
    bottom.setBounds(60, 60, 50, 50);  
    pane.add(bottom, new Integer(1));  
    pane.add(middle, new Integer(2));  
    pane.add(top, new Integer(3));  
  }  
  public static void main(String[] args) {  
      LayeredPaneExample panel = new  LayeredPaneExample();  
      panel.setVisible(true);  
  }  
}
