/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.javaswing;
import javax.swing.*;  
/**
 *
 * @author ACER
 */
public class TabbedPaneExample {

    JFrame f;

    TabbedPaneExample() {
        f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JTextArea ta = new JTextArea(300, 300);
        JPanel pa = new JPanel();
        pa.add(ta);
        JPanel p2 = new JPanel();
        JPanel p3 = new JPanel();
        JTabbedPane tp = new JTabbedPane();
        tp.setBounds(60, 60, 200, 200);
        tp.add("Main", pa);
        tp.add("Visit", p2);
        tp.add("Help", p3);
        f.add(tp);
        f.setSize(400, 400);
        f.setLayout(null);
        f.setVisible(true);
    }

    public static void main(String[] args) {
        new TabbedPaneExample();
    }
}
