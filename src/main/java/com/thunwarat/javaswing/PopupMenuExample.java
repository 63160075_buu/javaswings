/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.javaswing;

import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author ACER
 */
public class PopupMenuExample {

    PopupMenuExample() {
        final JFrame f = new JFrame("PopupMenu Example");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JMenuItem cut = new JMenuItem("Cut");
        JMenuItem copy = new JMenuItem("Copy");
        JMenuItem paste = new JMenuItem("Paste");
    }

    public static void main(String args[]) {
        new PopupMenuExample();
    }
}
