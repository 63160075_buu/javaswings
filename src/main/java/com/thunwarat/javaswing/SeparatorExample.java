/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.javaswing;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author ACER
 */
public class SeparatorExample {

    public static void main(String args[]) {
        JFrame f = new JFrame("Separator Example");
        f.setLayout(new GridLayout(0, 1));
        JLabel S1 = new JLabel("CHOICE Separator");
        f.add(S1);
        JSeparator sep = new JSeparator();
        f.add(sep);
        JLabel l2 = new JLabel("Take Separator");
        f.add(l2);
        f.setSize(500, 200);
        f.setVisible(true);
    }
}
