/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.javaswing;

import java.awt.Color;
import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author ACER
 */
public class RadioButtonExample extends JFrame implements ActionListener {

    JRadioButton A1, A2;
    JButton b;

    RadioButtonExample() {
        A1 = new JRadioButton("LIKE");
        A1.setBounds(100, 50, 100, 30);
        A1.setBackground(Color.PINK);
        A2 = new JRadioButton("NOT LIKE");
        A2.setBounds(100, 100, 100, 30);
        A2.setBackground(Color.YELLOW);
        ButtonGroup bg = new ButtonGroup();
        bg.add(A1);
        bg.add(A2);
        b = new JButton("click");
        b.setBounds(100, 150, 80, 30);
        b.setBackground(Color.GREEN);
        b.addActionListener(this);
        add(A1);
        add(A2);
        add(b);

        setSize(300, 300);
        setLayout(null);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (A1.isSelected()) {
            JOptionPane.showMessageDialog(this, "You are Male.");
        }
        if (A2.isSelected()) {
            JOptionPane.showMessageDialog(this, "You are Female.");
        }
    }

    public static void main(String args[]) {
        new RadioButtonExample();
    }
}
