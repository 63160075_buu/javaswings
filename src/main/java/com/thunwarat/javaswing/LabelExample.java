/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.javaswing;
import javax.swing.*;  
import java.awt.*;  
import java.awt.event.*;  
/**
 *
 * @author ACER
 */
public class LabelExample extends Frame implements ActionListener {

    JTextField tf;
    JLabel l;
    JButton jb;

    LabelExample() {
        tf = new JTextField();
        tf.setBounds(50, 50, 100, 20);
        l = new JLabel();
        l.setBounds(40, 100, 240, 10);
        jb = new JButton("Click IP");
        jb.setBounds(50, 150, 95, 30);
        jb.addActionListener(this);
        add(jb);
        add(tf);
        add(l);
        setSize(400, 400);
        //this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        try {
            String host = tf.getText();
            String ip = java.net.InetAddress.getByName(host).getHostAddress();
            l.setText("IP of " + host + " is: " + ip);
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    public static void main(String[] args) {
        new LabelExample();
    }
}
