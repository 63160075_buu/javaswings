/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.javaswing;

import java.awt.*;
import javax.swing.*;

/**
 *
 * @author ACER
 */
public class PanelExample {

    PanelExample() {
        JFrame f = new JFrame("Panel Example");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel panel = new JPanel();
        panel.setBounds(50, 90, 200, 200);
        panel.setBackground(Color.ORANGE);
        JButton b1 = new JButton("Button 1");
        b1.setBounds(50, 100, 80, 30);
        b1.setBackground(Color.RED);
        JButton b2 = new JButton("Button 2");
        b2.setBounds(100, 100, 80, 30);
        b2.setBackground(Color.PINK);
        panel.add(b1);
        panel.add(b2);
        f.add(panel);
        f.setSize(500, 500);
        f.setLayout(null);
        f.setVisible(true);
    }

    public static void main(String args[]) {
        new PanelExample();
    }
}
