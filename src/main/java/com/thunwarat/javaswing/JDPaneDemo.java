/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.javaswing;

import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;

/**
 *
 * @author ACER
 */
public class JDPaneDemo extends JFrame {

    public JDPaneDemo() {
        CustomDesktopPane desktopPane = new CustomDesktopPane();
        Container contentPane = getContentPane();
        contentPane.add(desktopPane, BorderLayout.CENTER);
        desktopPane.display(desktopPane);

        setTitle("JdesktopPane Example");
        setSize(300, 300);
        setVisible(true);
    }

    public static void main(String args[]) {
        new JDPaneDemo();
    }
}

class CustomDesktopPane extends JDesktopPane {

    int numFrames = 3, x = 30, y = 30;

    public void display(CustomDesktopPane dp) {
        for (int i = 0; i < numFrames; ++i) {
            JInternalFrame jframe = new JInternalFrame("Prayut Demo " + i, true, true, true, true);

            jframe.setBounds(x, y, 250, 90);
            Container c1 = jframe.getContentPane();
            c1.add(new JLabel("Sinovac Is Here"));
            dp.add(jframe);
            jframe.setVisible(true);
            jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            y += 85;
        }
    }
}
