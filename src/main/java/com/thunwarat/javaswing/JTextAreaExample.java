/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.javaswing;

import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author ACER
 */
public class JTextAreaExample implements ActionListener {

    JLabel Jl1, Jl2;
    JTextArea Area;
    JButton B;

    JTextAreaExample() {
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Jl1 = new JLabel();
        Jl1.setBounds(50, 25, 100, 30);
        Jl2 = new JLabel();
        Jl2.setBounds(160, 25, 100, 30);
        Area = new JTextArea();
        Area.setBounds(20, 75, 250, 200);
        B = new JButton("how?");
        B.setBounds(100, 300, 120, 30);
        B.addActionListener(this);

        f.add(Jl1);
        f.add(Jl2);
        f.add(Area);
        f.add(B);
        f.setSize(400, 400);
        f.setLayout(null);
        f.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        String text = Area.getText();
        String words[] = text.split("\\s");
        Jl1.setText("Words: " + words.length);
        Jl2.setText("Characters: " + text.length());
    }

    public static void main(String[] args) {
        new JTextAreaExample();
    }
}
