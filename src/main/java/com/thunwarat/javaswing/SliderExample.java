/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.javaswing;

import javax.swing.*;

/**
 *
 * @author ACER
 */
public class SliderExample extends JFrame {

    public SliderExample() {
        JSlider sd = new JSlider(JSlider.HORIZONTAL, 0, 40, 20);
        sd.setMinorTickSpacing(5);
        sd.setMajorTickSpacing(20);
        sd.setPaintTicks(true);
        sd.setPaintLabels(true);

        JPanel panel = new JPanel();
        panel.add(sd);
        add(panel);
    }

    public static void main(String s[]) {
        SliderExample frame = new SliderExample();
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
