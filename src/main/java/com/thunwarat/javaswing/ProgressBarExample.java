/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.javaswing;

import javax.swing.*;

/**
 *
 * @author ACER
 */
public class ProgressBarExample extends JFrame {

    JProgressBar Jus;
    int i = 0, num = 0;

    ProgressBarExample() {
        Jus = new JProgressBar(0, 2000);
        Jus.setBounds(30, 30, 160, 20);
        Jus.setValue(0);
        Jus.setStringPainted(true);
        add(Jus);
        setSize(260, 160);
        setLayout(null);
    }

    public void iterate() {
        while (i <= 2000) {
            Jus.setValue(i);
            i = i + 20;
            try {
                Thread.sleep(150);
            } catch (Exception e) {
            }
        }
    }

    public static void main(String[] args) {
        ProgressBarExample m = new ProgressBarExample();
        m.setVisible(true);
        m.iterate();
    }
}
