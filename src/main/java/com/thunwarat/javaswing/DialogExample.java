/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.javaswing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 *
 * @author ACER
 */
public class DialogExample {

    private static JDialog jd;

    DialogExample() {
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jd = new JDialog(f, "Dialog Example", true);
        jd.setLayout(new FlowLayout());
        JButton b = new JButton("Yes");
        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DialogExample.jd.setVisible(false);
            }
        });
        jd.add(new JLabel("Click button to continue."));
        jd.add(b);
        jd.setSize(300, 300);
        jd.setVisible(true);
    }

    public static void main(String args[]) {
        new DialogExample();
    }
}
