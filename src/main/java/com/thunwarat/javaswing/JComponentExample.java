/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.javaswing;
import java.awt.Color;  
import java.awt.Graphics;  
import javax.swing.JComponent;  
import javax.swing.JFrame;  
/**
 *
 * @author ACER
 */
class MyJComponent extends JComponent{
    public void paint(Graphics g) {  
        g.setColor(Color.YELLOW);  
        g.fillRect(40, 40, 110, 110);  
      }  
}  
public class JComponentExample {  
      public static void main(String[] arguments) {  
        MyJComponent com = new MyJComponent();  
        JFrame.setDefaultLookAndFeelDecorated(true);  
        JFrame frame = new JFrame("JComponent Example!!!");  
        frame.setSize(300,200);  
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
        frame.add(com);  
        frame.setVisible(true);  
      }  
}
