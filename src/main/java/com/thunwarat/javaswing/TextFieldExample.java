/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.javaswing;
import java.awt.Color;
import javax.swing.*;  
import java.awt.event.*;  
/**
 *
 * @author ACER
 */
public class TextFieldExample implements ActionListener {

    JTextField A1, A2, A3;
    JButton b1, b2;

    TextFieldExample() {
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        A1=new JTextField();  
        A1.setBounds(50,50,150,30); 
        A1.setBackground(Color.GREEN);
        A2=new JTextField();  
        A2.setBounds(50,100,150,30); 
        A2.setBackground(Color.PINK);
        A3=new JTextField();  
        A3.setBounds(50,150,150,30);  
        A3.setEditable(false);
        A3.setBackground(Color.RED);
        b1=new JButton("+");  
        b1.setBounds(50,200,50,50);
        b1.setBackground(Color.ORANGE);
        b2=new JButton("-");  
        b2.setBounds(120,200,50,50);
        b2.setBackground(Color.YELLOW);
        b1.addActionListener(this);  
        b2.addActionListener(this);  
        f.add(A1);f.add(A2);f.add(A3);f.add(b1);f.add(b2);  
        f.setSize(300,300);  
        f.setLayout(null);  
        f.setVisible(true); 
        }         
    public void actionPerformed(ActionEvent e) {  
        String s1=A1.getText();  
        String s2=A2.getText();  
        int a=Integer.parseInt(s1);  
        int b=Integer.parseInt(s2);  
        int c=0;  
        if(e.getSource()==b1){  
            c=a+b;  
        }else if(e.getSource()==b2){  
            c=a-b;  
        }  
        String result=String.valueOf(c);  
        A3.setText(result);  
    }
    public static void main(String[] args) {  
    new TextFieldExample();  
    }
}
