/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.javaswing;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

/**
 *
 * @author ACER
 */
public class ColorChooserExample extends JFrame implements ActionListener {

    JButton B;
    Container C;

    ColorChooserExample() {
        C = getContentPane();
        C.setLayout(new FlowLayout());
        B = new JButton("Color");
        B.addActionListener(this);
        C.add(B);
    }

    public void actionPerformed(ActionEvent e) {
        Color initialcolor = Color.RED;
        Color color = JColorChooser.showDialog(this, "Select a color", initialcolor);
        C.setBackground(color);
    }

    public static void main(String[] args) {
        ColorChooserExample ch = new ColorChooserExample();
        ch.setSize(500, 500);
        ch.setVisible(true);
        ch.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
