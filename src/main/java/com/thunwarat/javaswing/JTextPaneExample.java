/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.javaswing;
import java.awt.BorderLayout;  
import java.awt.Color;  
import java.awt.Container;  
import javax.swing.JFrame;  
import javax.swing.JScrollPane;  
import javax.swing.JTextPane;  
import javax.swing.text.BadLocationException;  
import javax.swing.text.Document;  
import javax.swing.text.SimpleAttributeSet;  
import javax.swing.text.StyleConstants;  
/**
 *
 * @author ACER
 */
public class JTextPaneExample {
    public static void main(String args[]) throws BadLocationException {  
        JFrame frame = new JFrame("JTextPane Example");  
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        Container cp = frame.getContentPane();  
        JTextPane pane = new JTextPane();  
        SimpleAttributeSet attributeSet = new SimpleAttributeSet();  
        StyleConstants.setBold(attributeSet, true);  
  
        
        pane.setCharacterAttributes(attributeSet, true);  
        pane.setText("welcome");  
  
        attributeSet = new SimpleAttributeSet();  
        StyleConstants.setItalic(attributeSet, true);  
        StyleConstants.setForeground(attributeSet, Color.PINK);  
        StyleConstants.setBackground(attributeSet, Color.WHITE);  
  
        Document doc = pane.getStyledDocument();  
        doc.insertString(doc.getLength(), "to Java ", attributeSet);  
  
        attributeSet = new SimpleAttributeSet();  
        doc.insertString(doc.getLength(), "world", attributeSet);  
  
        JScrollPane scrollPane = new JScrollPane(pane);  
        cp.add(scrollPane, BorderLayout.CENTER);  
  
        frame.setSize(500, 400);  
        frame.setVisible(true);  
      }  
}
