/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.javaswing;

import java.awt.Color;
import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author ACER
 */
public class CheckBoxExample {

    CheckBoxExample() {
        JFrame f = new JFrame("CheckBox Example");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        final JLabel label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setSize(400, 100);
        JCheckBox checkbox1 = new JCheckBox("Yes");
        checkbox1.setBounds(150, 100, 50, 50);
        checkbox1.setBackground(Color.GREEN);
        JCheckBox checkbox2 = new JCheckBox("Not");
        checkbox2.setBounds(150, 150, 50, 50);
        checkbox2.setBackground(Color.RED);
        f.add(checkbox1);
        f.add(checkbox2);
        f.add(label);
        checkbox1.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                label.setText("Yes Checkbox: "
                        + (e.getStateChange() == 1 ? "checked" : "unchecked"));
            }
        });
        checkbox2.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                label.setText("Not Checkbox: "
                        + (e.getStateChange() == 1 ? "checked" : "unchecked"));
            }
        });

        f.setSize(400, 400);
        f.setLayout(null);
        f.setVisible(true);
    }

    public static void main(String args[]) {
        new CheckBoxExample();
    }
}
