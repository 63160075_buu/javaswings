/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.javaswing;

import javax.swing.*;

/**
 *
 * @author ACER
 */
public class ToolTipExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("password field example");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPasswordField value = new JPasswordField();
        value.setBounds(100, 100, 100, 30);
        value.setToolTipText("enter your password");
        JLabel l1 = new JLabel("password:");
        l1.setBounds(20, 100, 80, 30);

        f.add(value);
        f.add(l1);
        f.setSize(300, 300);
        f.setLayout(null);
        f.setVisible(true);
    }
}
