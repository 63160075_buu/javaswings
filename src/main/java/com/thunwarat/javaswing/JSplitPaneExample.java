/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.javaswing;

import java.awt.FlowLayout;
import java.awt.Panel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JSplitPane;

/**
 *
 * @author ACER
 */
public class JSplitPaneExample {

    private static void createAndShow() {

        final JFrame frame = new JFrame("JSplitPane Example");

        frame.setSize(400, 400);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.getContentPane().setLayout(new FlowLayout());
        String[] option1 = {"Prayut", "Prawit", "Tae", "Parenaa", "Mhorpra"};
        JComboBox box1 = new JComboBox(option1);
        String[] option2 = {"Yes", "No", "Ok", "Thx.", "Oh no!!!"};
        JComboBox box2 = new JComboBox(option2);
        Panel panel1 = new Panel();
        panel1.add(box1);
        Panel panel2 = new Panel();
        panel2.add(box2);
        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, panel1, panel2);

        frame.getContentPane().add(splitPane);
    }

    public static void main(String[] args) {

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShow();
            }
        });

    }
}
