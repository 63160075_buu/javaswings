/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.javaswing;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author ACER
 */
public class TreeExample {

    JFrame frame;

    TreeExample() {
        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        DefaultMutableTreeNode Name = new DefaultMutableTreeNode("Name");
        DefaultMutableTreeNode LIKE = new DefaultMutableTreeNode("LIKE");
        DefaultMutableTreeNode font = new DefaultMutableTreeNode("font");
        Name.add(LIKE);
        Name.add(font);
        DefaultMutableTreeNode Prayut = new DefaultMutableTreeNode("Prayut");
        DefaultMutableTreeNode Prawit = new DefaultMutableTreeNode("Prawit");
        DefaultMutableTreeNode Tae = new DefaultMutableTreeNode("Tae");
        DefaultMutableTreeNode Pareenaa = new DefaultMutableTreeNode("Pareenaa");
        LIKE.add(Prayut);
        LIKE.add(Prawit);
        LIKE.add(Tae);
        LIKE.add(Pareenaa);
        JTree jt = new JTree(Name);
        frame.add(jt);
        frame.setSize(300, 300);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new TreeExample();
    }
}
